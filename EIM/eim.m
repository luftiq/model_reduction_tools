classdef eim < matlab.mixin.Copyable
    % weighted EIM class
    % Builds EIM of function g(x,y) = \sum_m \lambda_m(y) \phi_m(x)
    properties
        w % Weight
        V % Discrete x-space
        g % Function handle
        x % Chosen x-points indices ==> points V(J,:)
        mu % Chosen y-points
        q % Basis q_j(x)
        M % Number of terms M
        lastM
        err % L^inf error at each M
        traincount = 0
        heim
        verbose
    end
    
    methods(Static)
        function obj = loadobj(s)
            if isstruct(s)
                newObj = eim;
                newObj.w  = s.w;
                newObj.V = s.V;
                newObj.g = s.g;
                newObj.x = s.x;
                newObj.mu = s.mu;
                newObj.M = s.M;
                newObj.traincount = s.traincount;
                newObj.lastM = s.lastM;
                newObj.err = s.err;
                if s.verbose                    
                    figure
                    newObj.heim = semilogy(1:length(s.err), s.err(1:end),'-x');
                    title('EIM error')
                    xlabel('Number of terms')
                    ylabel('Absolute error')
                end
                obj = newObj;
            else
                obj = s;
            end
        end
        
    end
    
    methods
        function obj = eim(V, g, rootmu, varargin)
            if nargin > 0
                p = inputParser;
                p.FunctionName = 'EIM';
                p.PartialMatching = false;
                addRequired(p,'V',@(x) validateattributes(x, {'numeric'},{'2d'}));
                addRequired(p,'g', @(x) validateattributes(x, {'function_handle'},{}));
                addRequired(p,'rootmu',@(x) validateattributes(x, {'numeric'},{'vector'}));
                addParameter(p,'verbose',true, @(x) validateattributes(x, {'logical'},{}));
                addParameter(p,'w',@(x) 1);
                parse(p, V, g, rootmu, varargin{:});
                obj.g = p.Results.g;
                obj.V = p.Results.V;
                obj.w = p.Results.w;
                obj.M = 1;
                obj.lastM = obj.M;
                r = obj.g(obj.V, p.Results.rootmu);
                [~, ind] = max(abs(r));
                obj.x(obj.M) = ind(1);
                obj.q(:,obj.M) = r/r(obj.x(obj.M));
                obj.mu = rootmu;
                obj.verbose = p.Results.verbose;
                if obj.verbose
                    figure
                    obj.heim = semilogy(nan,'-x');
                    title('EIM error')
                    xlabel('Number of terms')
                    ylabel('Absolute error')
                end
            end           
        end
        
        function s = saveobj(obj)
            s.w  = obj.w;
            s.V = obj.V;
            s.g = obj.g;
            s.x = obj.x;
            s.mu = obj.mu;
            s.M = obj.M;
            s.traincount = obj.traincount;
            s.lastM = obj.lastM;
            s.err = obj.err;
            s.verbose = obj.verbose;
        end
        
        function loadValues(obj, s)
            fields = fieldnames(s);
            for i=1:length(fields)
                field = fields{i};
                obj.(field) = s.(field);
            end
        end
        
        function train(obj, xi_train, Mmax, tol)
            obj.lastM = obj.M;
            obj.traincount = obj.traincount + 1;
            [eM, newmu] = obj.solveopt(xi_train);
            obj.err(obj.M) = eM;
            thetaM = obj.coeff(newmu);
            r = obj.g(obj.V, newmu) - obj.q*thetaM;
            while obj.M < Mmax && eM > tol
                obj.M = obj.M + 1;
                obj.mu = [obj.mu newmu];
                [~, ind] = max(abs(r));
                obj.x(obj.M) = ind(1);
                obj.q(:, obj.M) = r/r(obj.x(obj.M));
                [eM, newmu] = obj.solveopt(xi_train);
                obj.err = [obj.err, eM];
                thetaM = obj.coeff(newmu);
                r = obj.g(obj.V, newmu) - obj.q*thetaM;
            end
            if obj.verbose
                % Spew some info
                fprintf('Performed %d EIM iterations on training set of size N=%d\n', obj.M - obj.lastM, size(xi_train, 2));
                set(obj.heim, 'XData', 1:length(obj.err), 'YData', obj.err(1:end));
                drawnow
            end
        end
        
        function [eM, newmu] = solveopt(obj, xi_train)
            Nxi = size(xi_train, 2);
            err = zeros(Nxi, 1);
            wk = err;
            for k = 1: Nxi
                gmu = obj.g(obj.V, xi_train(:, k));
                thetaM = obj.coeff(xi_train(:, k));
                geim = obj.q * thetaM;
                err(k) = norm(gmu - geim,'inf');
                wk(k) = obj.w(xi_train(:,k));
            end
            [~, muind] = max(wk.*err);
            eM = err(muind);
            newmu = xi_train(:, muind);
        end
        
        function enrich(obj, newmu)
            obj.traincount = obj.traincount + 1;
            dimDiff = size(obj.mu,1) - size(newmu,1);
            obj.mu = [obj.mu [newmu;zeros(dimDiff,1)]];
            thetaM = obj.coeff(newmu);
            r = obj.g(obj.V, newmu) - obj.q*thetaM;
            [~, ind] = max(abs(r));
            obj.M = obj.M + 1;
            obj.x(obj.M) = ind(1);
            obj.q(:,obj.M) = r/r(obj.x(obj.M));
        end
        
        function thetaM = coeff(obj, mu, varargin)
            if nargin > 2
                N = varargin{1};
            else
                N = obj.M;
            end
            try
                funn = obj.g(obj.V, mu, obj.x);
            catch
                funn = obj.g(obj.V, mu);
                funn = funn(obj.x,:);
            end
            interpmat = obj.q(obj.x,:);
            thetaM = interpmat(1:N, 1:N) \ funn(1:N);
        end
        
        function [thetaM, feim, error] = evaluate(obj, mu, varargin)
            if nargin > 2
                N = varargin{1};
            else
                N = obj.M;
            end
            funn = obj.g(obj.V, mu);
            thetaM = obj.coeff(mu, N);
            feim = obj.q(:, 1:N) * thetaM;
            error = norm(feim - funn,'inf');
        end
        
        function [maxerr, meanerr, meanerrlogexp] = test(obj, xi_test)
            Nxi = size(xi_test,2);
            err = zeros(Nxi, obj.M);
            wk = err;
            for k = 1: Nxi
                gmu = obj.g(obj.V, xi_test(:, k));
                for i = 1:obj.M
                    thetaM = obj.coeff(xi_test(:, k), i);
                    geim = obj.q(:, 1:i) * thetaM;
                    err(k, i) = norm(gmu - geim,'inf');
                    wk(k, i) = obj.w(xi_test(:,k));
                end
            end
            for i=1:obj.M
                [me, ~] = max(wk(:,i).*err(:,i));
                maxerr(i) = me;
                meanerr(i) = sum(wk(:,i).*err(:,i))/Nxi;
                meanerrlogexp(i) = exp(mean(log(wk(:,i).*err(:,i))));
            end
            figure            
            semilogy(1:obj.M, maxerr);     
            hold on
            semilogy(1:obj.M, meanerr);
            semilogy(1:obj.M, meanerrlogexp); 
            legend('maxerr','meanerr','meanerrlogexp');
            title('EIM error over test set')
            xlabel('Number of terms')
            ylabel('Error')     
        end
        
        function addDim(obj)
            [dim,np] = size(obj.mu);
            obj.mu = [obj.mu;zeros(1,np)];
        end
    end
end