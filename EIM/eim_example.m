% Example of empirical interpolation
clear myeim
myeim = eim(Omega_h,foo,Xi_train(1)); % initialize eim
myeim.train(Xi_train',maxiter,tol); % train 
Xi_test = rand(10,1);
[maxerr, meanerr, meanerrlogexp] = myeim.test(Xi_test'); % test eim over test set

 
 
 
 